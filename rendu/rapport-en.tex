\documentclass[11pt, a4paper, english, titlepage]{article}

\usepackage[T1]{fontenc} % pour la police
\usepackage{palatino}

\usepackage[english]{babel} % pour la langue

\usepackage{graphicx} % pour les images
\usepackage{float}
\usepackage{subcaption}
\graphicspath{{images/}}

\usepackage[margin=2cm]{geometry} % pour les marges

\usepackage{parskip} % pour sauter une ligne apres un paragraphe
\setlength{\parskip}{\baselineskip}

\usepackage{hyperref} % pour les liens
\hypersetup{colorlinks=true, linkcolor=black,  urlcolor=blue, citecolor=blue}

% -----------------------------------------------------------

\title{Rapport SAE\\BUT3 DACS\\Groupe 5}
\author{Paul Bressolles\\Théo Riol\\Philemon Droues\\Guilhem Fauré\\Clément Saury}
\date{}

\begin{document}
	\maketitle
	\thispagestyle{empty}
	
	% -----------------------------------------------------------
	
	\thispagestyle{empty}
	\tableofcontents
	\listoffigures
	\listoftables
	
	% -----------------------------------------------------------
	
	\newpage
	\pagenumbering{arabic}
	\addcontentsline{toc}{section}{Introduction}
	\section*{Introduction}
	Trisomie 21 Haute-Garonne, a nonprofit dedicated to help people suffering from the Down’s syndrome, is about to perform a global transformation of its activities. In order to support this important initiative, the nonprofit launches a call for tenders that could digitalize the whole organization’s infrastructure. This ambitious project is composed of several keystones, ranging from the deployment of a full-fledged computer network, to the setup of essentials services, also including data protection and tasks automation.
	
	The ultimate goal of this initiative is to modernize the activities of Trisomie 21 Haute-Garonne. This will imply a significant upgrade of the internal communication, of the process’ efficiency and an improved guarantee of security for sensible data. By deploying advanced technologies, the nonprofit wants to maximize his positive impact and better serve his purpose.
	
	In the next sections, we’ll explore the various challenges and opportunities brought by this huge computerization project, and the expected benefits for Trisomie 21 Haute-Garonne, his employees and more importantly the children. We will also dive into the technical details of the infrastructure and the services that will be brought up to answer the specific needs of the nonprofit. Particularly, we will explain the network’s configuration, the installation of essential services, the data protection and the processes’ automation.
	
	This project stands as a major shift for Trisomie 21 Haute-Garonne, an opportunity to reinforce his operation’s efficiency, his data protection, and above all, reinforce his capacity to always perform better in his main mission. Let’s follow along our deep exploration of this particularly interesting digital shift to learn more about the keystones, the faced challenges and the expected benefits.
	
	\addcontentsline{toc}{section}{Additional information}
	\section*{Additional information}
	The following schemes were made with the only drawing tool \href{https://excalidraw.com}{Excalidraw},
	and this present document was written in LaTeX using the online writing tool \href{https://overleaf.com}{Overleaf}. 
	Images, simulations, and scripts are hosted on our \href{https://gitlab.com/pdroues/s5-b01-g5}{GitLab repository}.
	
	% -----------------------------------------------------------
	
	\newpage
	\section{Network}
	\subsection{The network}
	\subsubsection{Hardware}
	The available equipment consists of :
	\begin{itemize}
		\item 5 routers with 3 WAN interfaces
		\item 2 routers with 2 WAN and 2 serial interfaces
		\item 10 48-port switches
		\item 1 server
		\item 30 Ethernet cables and 2 serial cables
	\end{itemize}
	\subsubsection{Network topology}
	During the conception of the network’s topology, we went through several prototypes. After some precision about the available hardware, we adapted our topology accordingly.
	
	The topology includes wired network only, as we don’t plan on deploying wireless.
	\begin{figure}[H]
		\centering
		\includegraphics[height=1.45cm]{images/legende.png}
		\caption{Legend of topology}
	\end{figure}
	\paragraph{Prototype n°1}
	We have opted for maximum redundancy without taking into account the difficulty of configuration. The 3 entry points do not respect the principle of complete mediation and do not constitute a WAN zone. Site 2 is the remote site, accessing the main site and site 1 via the Internet.
	
	\paragraph{Prototype n°2}
	We have chosen a maximum redundancy without taking into account the difficulty of configuration.
	The 3 entry points do not respect the complete mediation principle and do not constitute a WAN zone. Site 2 is the remote site, accessing the two others sites through Internet.
	
	\paragraph{Prototype n°3}
	The aim of this iteration was to simplify infrastructure management by moving away from redundancy. We respect the principle of complete mediation by reducing the number of entries into the sites to a minimum. However, the WAN does not allow for any redundancy.
	
	\paragraph{Prototype n°4}
	This last version is a compromise between ease of configuring the network infrastructure and ease of maintaining it. Redundancy is provided by routers between the two sites.
	
	\begin{figure}[H]
		\centering
		
		\begin{subfigure}{0.49\textwidth}
			\includegraphics[width=\linewidth]{images/topologie_1.png}
			\caption{Topology n°1}
		\end{subfigure}
		\begin{subfigure}{0.49\textwidth}
			\includegraphics[width=\linewidth]{images/topologie_2.png}
			\caption{Topology n°2}
		\end{subfigure}
		\begin{subfigure}{0.49\textwidth}
			\includegraphics[width=\linewidth]{images/topologie_3.png}
			\caption{Topology n°3}
		\end{subfigure}
		\begin{subfigure}{0.49\textwidth}
			\includegraphics[width=\linewidth]{images/topologie_4.png}
			\caption{Topology n°4}
		\end{subfigure}
		
		\caption{Topology iteration}
	\end{figure}
	\paragraph{Final version}
	Below is the final version of the topology, to which we brought some changes.
	We have moved the WAN's internet input to our main site, rebalanced the number of switches between sites and created a pseudo DMZ (more on this later). We also have reinforced WAN redundancy.
	\begin{figure}[H]
		\centering 
		\includegraphics[height=7cm]{images/topologie_finale.png}
		\caption{Final version of topology}
	\end{figure}
	\subsubsection{Client IP addressing}
	To be as compatible as possible, we're going to configure our network dual-stacked, with IPv4 and IPv6. These networks are VLANs, which allows us to separate the flows between the two types of people who are present (managers and project managers). 
	
	The masks were chosen to allow the networks to evolve by connecting more clients than required. Furthermore, the networks of the main site and site 1 follow each other to enable surneting if necessary.
	\begin{table}[H]
		\centering
		\begin{tabular}{|p{4cm}|c|c|c|c|c|c|} \hline
			Title & Users & Net IP & Mask & IPs & First IP & Last IP \\ \hline
			main site: manager & 30 & 192.168.0.0 & 26 & 62 & 192.168.0.1 & 192.168.0.62 \\
			main site: project mgr & 90 & 192.168.1.0 & 24 & 254 & 192.168.1.1 & 192.168.0.254 \\
			site 1: manager & 15 & 192.168.2.0 & 27 & 30 & 192.168.2.1 & 192.168.2.30 \\
			site 1: project manager & 60 & 192.168.3.0 & 25 & 126 & 192.168.3.1 & 192.168.3.126 \\
			site 2: manager & 15 & 192.168.4.0 & 27 & 30 & 192.168.4.1 & 192.168.4.30 \\
			site 2: project manager & 60 & 192.168.5.0 & 25 & 126 & 192.168.5.1 & 192.168.5.126 \\ \hline
		\end{tabular}
		\caption{IPv4 client IP addressing}
	\end{table}
	
	For IPv6 addressing, we will use Unique Local IPs. As these IPs aren’t routable over Internet, we will need to set up NAT (Network Address Translation). In this present document, we use 2001::/32  addresses for documentation only, so the addresses will have to be set to proper ones for real usage.
	\begin{table}[H]
		\centering
		\begin{tabular}{|c|c|c|} \hline
			Title & Users & Net IP \\ \hline
			main site: manager & 30 & 2001:1:: \\
			main site: project manager & 90 & 2001:2:: \\
			site 1: manager & 15 & 2001:3:: \\
			site 1: project manager & 60 & 2001:4:: \\
			site 2: manager & 15 & 2001:5:: \\
			site 2: project manager & 60 & 2001:6::  \\ \hline
		\end{tabular}
		\caption{IPv6 client IP addressing}
	\end{table}
	\subsubsection{WAN IP addressing}
	Below is a graphical version of the WAN IPv4 and IPv6 addressing plan. The masks used in IPv4 are /30 to save addresses. In IPv6 we use addresses in 2001:db8:X::Y/64 as an example, in a real case we would use valid addresses.
	
	\begin{figure}[H]
		\centering
		\begin{subfigure}{0.9\textwidth}
			\includegraphics[width=\linewidth]{images/wan_ipv4.png}
			\caption{IPv4 addressing plan}
		\end{subfigure}
		
		\begin{subfigure}{0.9\textwidth}
			\includegraphics[width=\linewidth]{images/wan_ipv6.png}
			\caption{IPv6 addressing plan}
		\end{subfigure}
		\caption{WAN addressing plans}
	\end{figure}
	
	\subsubsection{STP}
	The Spanning Tree Protocol avoids loops in an Ethernet network by deactivating certain redundant paths, thus ensuring network stability and predicting congestion problems. In our case, we're going to use it mainly to avoid loops in our network. Indeed, our configuration offers little resilience. This protocol is configured at switch level, where a "master" must be designated. We've opted for the switch connected to the router.
	
	\subsection{Security}
	\subsubsection{Firewall}
	We set up two firewalls, the first at the entrance of the main site, the second at the entrance of the second site. These will filter all incoming and outgoing traffic. These firewalls are stateless on the network layer.
	
	The firewall on the main site is configured to let in all traffic from the site, and to block all outgoing requests not passing through the company's proxy servers.
	The firewall on the second site is configured to let in only traffic coming from or going to the main site.
	
	We're going to use the firewall system built into Cisco routers to apply the economy of mechanisms.
	
	\subsubsection{VPN}
	The VPN we're going to use is a network-layer VPN that will enable us to create a secure tunnel between remote sites. We won't use one between the main site and the second site, as the WAN is ours and we want to save router resources.
	
	We're going to use the VPN system built into Cisco routers to apply the economy of mechanisms.
	
	\subsubsection{DMZ}
	The DMZ is an extremely important security element. Unfortunately, due to a lack of resources, the DMZ cannot meet all our security requirements. In fact, as we'll see in the section on improvements, the DMZ is present in our internal network, which drastically increases the attack surface.
	
	\subsubsection{VLAN}
	As mentioned previously, the two groups of people are on separate networks, for several reasons: to minimize the number of people on a network to limit collision damage, and to separate flows to avoid data sniffing. To avoid creating two physical networks, which would double costs, we're going to use VLANs, which are configured at switch level.
	
	\subsection{Routing}
	
	\subsubsection{Internet}
	We will use BGP to route packets out of our network to the internet. Indeed, BGP is the only Exterior Gateway Protocol (EGP) routing protocol.
	
	\subsubsection{WAN}
	To route packets across the WAN, we have chosen to use the OSPF routing protocol. Indeed, OSPF offers several advantages, such as very fast convergence times and near-instant change detection, unlike other alternative protocols like RIP.
	
	We are not using EIGRP, which is slightly more efficient, in order to avoid locking ourselves into the Cisco ecosystem.
	
	We will configure it with a single zone since the number of routers is relatively low.
	% -----------------------------------------------------------
	
	\newpage
	\section{The services}
	To host our services, we have a physical machine.
	The services will be containerized, so that we can easily move to a cloud environment and save resources. We won't be using an orchestrator initially.
	
	The containerization technology used will be \emph{Docker}, installed on a Linux server. We chose \emph{Debian} for its stability and free availability, despite its relatively short life cycle (5 years).
	
	We'll be prioritizing pre-existing official \emph{Docker} images as much as possible, as this will relieve us of the need to create and maintain images, saving us a considerable amount of time and effort.
	
	The services to be deployed fall into two broad categories. Some services enable the technical operation of the infrastructure (DHCP, DNS...), while others provide functionalities and workflows for users (Nextcloud, business applications...).
	
	In the enhancement section, we'll look at other services we could deploy.
	
	\subsection{Backup}
	With the planned hardware, the question of backup is a difficult one. We plan to use a cloud to store the files, which implies that the only data to be kept would be on the cloud server. But the cloud server is a container on the only physical server. Backing up a machine to itself is pointless and doesn't respect the data security good principles (like the 3-2-1-1 rule), so we won't be putting a backup solution on it.
	
	The only alternative to make up for this lack of resources is to make backups on external hard disks or magnetic tape, then move them to another site or, at the very least, into a safe.
	
	\subsection{DHCP}
	We're going to use \emph{\href{https://www.isc.org/kea/}{Kea}} as our DHCP server, which has \href{https://cloudsmith.io/~isc/repos/docker/packages/}{official \emph{Docker} images} for IPv4, IPv6 and DynDNS.
	
	Kea is chosen as the DHCP server currently recommended by the Internet Systems Consortium, replacing the deprecated \emph{\href{https://www.isc.org/dhcp/}{ISC DHCP Server}} since 2022.
	
	\subsection{DNS}
	For our DNS we'll be using the \emph{\href{https://www.isc.org/bind/}{Bind9}} server through the official \emph{Docker} images maintained by ISC, the developer of \emph{Bind9}.
	
	We'll be using it, as it \emph{Bind9} is the most widely used open source DNS server and its replacement \emph{Bind10} is not yet considered usable.
	
	\subsection{Business application}
	The business application consists of a PHP-capable web server and an SQL database. First, we'll need to create our \emph{Docker} web server and database images with \emph{Dockerfiles}, then deploy everything with \emph{Docker Compose} via a dockercompose.yml file.
	
	We'll be using \emph{Apache2} as the web server and \emph{MySQL} as the database, as these are the software packages for which the application was originally designed. We'll be using the official images \href{https://hub.docker.com/_/httpd}{Apache} and \href{https://hub.docker.com/_/mysql}{MySQL}.
	
	\subsection{Nextcloud}
	As previously mentioned, we'll be using a cloud for file storage.
	
	We've chosen to use \emph{Nextcloud} which is a self-hosted cloud solution with strong community support, open source and featuring numerous functionalities (mail client, chat, videoconferencing...) in addition to file storage and sharing. What's more, it's recommended by the \href{https://code.gouv.fr/sill/detail?name=NextCloud}{\emph{SILL}}.
	
	We're deploying it with Docker for the reasons outlined above, via the \href{https://hub.docker.com/_/nextcloud}{official image} using MySQl as our database.
	
	\subsection{Directory}
	To centralize account management, we're going to use an LDAP server. We've chosen the \emph{OpenLDAP} directory, particularly for its modularity thanks to the addition of schema, enabling it to adapt to many situations. We'll need to create a specific \emph{Docker} image, which will interface with other services using LDAP for authentication, such as the mail service or Nextcloud.
	
	\subsection{Email}
	To enable users to communicate in a secure and sovereign way, we will provide them with a mail service. Current mail services use a large number of architectural elements, including an IMAP server, an SMTP server, an anti-spam and anti-virus system and an authentication system. We've therefore opted for a \emph{Docker} image \href{https://hub.docker.com/r/mailserver/docker-mailserver}{project} including all these necessary services, the most important of which are \emph{Dovecot}, \emph{Postfix} and \emph{SpamAssasin}.
	
	% -----------------------------------------------------------
	
	\newpage
	\section{The budget}
	
	We're going to look at the budget required to build the system explained above. The budget is susceptible to change considerably between the time we wrote these lines and the time you're reading them. The price of the equipment should fall over time, and it might even be possible to buy second-hand to reduce costs. Here we are assuming that we could buy from a specialist retailer.
	
	The 'human' cost will depend on the number of services the company chooses, which is why we're going to try and give a number of hours per service, although this is not representative because some services make it easier to install other applications, or conversely, they make it more complex.
	
	Finally, some information, such as the criticality of certain applications, has not been communicated to us, which complicates the forecasting of a \emph{BCP} or \emph{DRP} and therefore of a corresponding budget.
	
	\subsection{Hardware}
	
	We'll start with the cost of the network equipment, so we'll need 10 manageable 48-port switches at 1Gbs. The price of this type of equipment is around 1,000€/unit, for example the Catalyst 3560 48-port model.
	
	Next we need to think about the routers, of which we'll need 7, which must have at least 2 1Gbs Ethernet interfaces, an integrated firewall and a VPN. The price of this type of equipment is around €500/unit, for example the CISCO 2911 router.
	
	To finish off the network section, we need cables to interconnect the above-mentioned equipment with each other or with client workstations. Given that the network equipment we're using is limited to 1Gbs and that the association's offices aren't very large, we can use category 5e Ethernet cable. The total distance of the cables should be around 860 meters, which would cost us a total of €238.
	
	The last point on hardware concerns the server. We can use any computer to save costs, but to take advantage of features that improve redundancy or server management we need to buy a professional server, which costs a lot more.
	
	\subsection{Human resources}
	
	Regarding the "Human" part of the budget, we are considering a total of 86 hours of work for
	installing and configuring the entire infrastructure. One hour of work is
	is estimated at 17€, giving us a total of 1462€ per person. We will then multiply this
	total per person by 5, which corresponds to the number of people in the team responsible for
	setting up the infrastructure.
	
	(For more details on the budget, see the attached estimate).
	
	% -----------------------------------------------------------
	
	\newpage
	\section{Updates and Upgrades}
	To begin with, let's look at the improvements we could make and what they could add.
	
	\subsection{DMZ}
	The current DMZ doesn't meet the right security criteria, due to a lack of hardware. We therefore propose to purchase a new router at a later date, which we'll position between the Internet access and the main zone router.
	This will enable us to reduce the attack surface, better filter flows and apply a protocol break to analyze them.
	
	\begin{figure}[H]
		\centering
		\includegraphics[height=7cm]{best_dmz.png}
		\caption{Example of DMZ}
	\end{figure}
	
	\subsection{The services}
	
	Right now, we're offering the minimum number of services, but we could add many more that would make life easier for users and administrators.
	
	What's more, some services are absolutely essential and need to be added as soon as possible, such as the backup server, which needs to be one or more different machines to ensure data security.
	
	Here is a non-exhaustive list of services that could be added:
	
	\begin{itemize}
		\item password manager
		\item HR software
		\item showcase website
		\item backup server
		\item fleet management software
		\item supervision software
		\item printing server
	\end{itemize}
	
	% -----------------------------------------------------------
	
	\newpage
	\addcontentsline{toc}{section}{Annexes}
	\section*{Annexes}
	\begin{figure}[H]
		\centering 
		\includegraphics[height=17.5cm]{images/Devis SAE infra.png}
		\caption{Devis}
	\end{figure}
	
\end{document}