# Site Principal
## IPV4
| utilité | adresse réseau | masque | clients possibles | clients attendu | ip routeur |
| --- | --- | --- | --- | --- | --- |
| reseau site principal pour responsable | 192.168.0.0 | 26 | 62 | 30 | xx |
| reseau site principal pour charge de mission | 192.168.1.0 | 24 | 254 | 90 | xx |

## IPV6

# Site 1
## IPV4
| utilité | adresse réseau | masque | clients possibles | clients attendu | ip routeur |
| --- | --- | --- | --- | --- | --- |
| reseau site 1 pour responsable | 192.168.2.0 | 27 | 30 | 15 | xx |
| reseau site 1 pour responsable | 192.168.3.0 | 25 | 126 | 60 | xx |

# Site 2 (distant)
## IPV4
| utilité | adresse réseau | masque | clients possibles | clients attendu | ip routeur |
| --- | --- | --- | --- | --- | --- |
| reseau site 2 pour responsable | 192.168.4.0 | 27 | 30 | 15 | xx |
| reseau site 2 pour responsable | 192.168.5.0 | 25 | 126 | 60 | xx |

# WAN
## IPV4









---
tous ces réseau sont des vlan et le routeur sera la dernier adresse ip disponible

| réseau | nbr client | IP | masque | nbr hotes possibles | 1er IP | last IP |
| --- | --- | --- | --- | --- | --- | --- |
| site 1 responsable | 30 | 192.168.0.0 | 26 | 62 | xxx | xxx |
| site 1 charge mission | 90 | 192.168.1.0 | 24 | 254 | xxx | xxx |
| site 2 responsable | 15 | 192.168.2.0 | 27 | 30 | xxx | xxx |
| site 2 charge mission | 60 | 192.168.3.0 | 25 | 126 | xxx | xxx |
| site 3 responsable | 15 | 192.168.4.0 | 27 | 30 | xxx | xxx |
| site 3 charge mission | 60 | 192.168.5.0 | 25 | 126 | xxx | xxx |